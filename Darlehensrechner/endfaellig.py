#!/usr/bin/python

import sys
import locale
from decimal import *

def printVersionInformation():
    line1 = "Willkommen zum Darlehensrechner"
    line2 = "Version: 0.1 vom 07.09.2017"
    line3 = "Fehler bitte an: adams@tbs1.de"
    print("**********************************************\n" \
          "**", line1.center(40), "**\n" \
          "**", line2.center(40), "**\n" \
          "**", line3.center(40), "**\n" \
          "**********************************************\n")

def printUsage():
    print('Usage:\n./darlehen.py [Leihbetrag] [Zinssatz] [Jahre]')


def main():
    locale.setlocale(locale.LC_ALL, 'deu_deu')

    if 4 != len(sys.argv):
        printUsage();
        return;

    printVersionInformation()

    leihbetrag = Decimal(sys.argv[1])
    zinssatz = Decimal(sys.argv[2]) / 100
    jahre = int(sys.argv[3])

    jahreszinsen = leihbetrag * zinssatz
    gesamtzinsen = jahreszinsen * jahre
    darlehensbetrag = leihbetrag + gesamtzinsen

    print('Leihbetrag: {0}€'.format(leihbetrag))
    print('Zinssatz: {0}%'.format(zinssatz))
    print('Jahre: {0}'.format(jahre))
    print('Jahreszinsen: {0}€'.format(jahreszinsen))
    print('Gesamtzinsen: {0}€'.format(gesamtzinsen))
    print('Darlehensbetrag: {0}€'.format(darlehensbetrag))

if __name__ == "__main__":
    main()