import locale
from decimal import Decimal

# Set the localization to german
locale.setlocale(locale.LC_ALL, "de-DE")

print("Willkommen zum AnnuitätendarlehenRechner!")

sum = input("Bitte geben Sie die Kreditsumme in Euro ein: ")
if(not sum.isdecimal() or Decimal(sum) <= 0):
    print("Der Wert muss eine Zahl größer als 0 sein")
    exit(1)
sum = Decimal(sum)

rate = input("Bitte geben Sie den Zinssatz in Prozent ein: ")
if(not rate.isdecimal() or Decimal(rate) <= 0):
    print("Der Wert muss eine Zahl größer als 0 sein")
    exit(1)
rate = Decimal(rate) / 100

if(rate > 10):
    print("Achtung: Der Zinssatz beträgt über 10%")

duration = input("Bitte geben Sie die Dauer in Jahren ein: ")
if(not duration.isnumeric() or int(duration) <= 0):
    print("Der Wertmuss eine Zahl größer als 0 sein")
duration = int(duration)

annuity = sum * (((1 + rate)**duration) * rate) / (((1 + rate)**duration) - 1)
print("Ihre Annuität beträgt {0:}€".format(round(annuity, 2)))

interest = annuity * duration - sum
print("Ihre Zinsen betragen: {0:}€".format(round(interest, 2)))

print("Programm beendet sich korrekt")
